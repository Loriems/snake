<?php
namespace Layout;

require_once 'Snake/Compiler.php';

use Layout\Snake\Compiler;

class Snake
{
   protected static $view_vars = array();

   protected static function source($view)
   {
      $view_file = "views/sources/$view.snake.php";

      if(!file_exists($view_file))
         trigger_error("Snake: template $view_file do not exists", E_USER_ERROR);

      return Compiler::parse(file_get_contents($view_file));
   }

   protected static function template_include($view)
   {
      self::draw($view, self::$view_vars);
   }

   protected static function make($view, $vars)
   {
      self::$view_vars = $vars;
      $source = self::source($view);

      ob_start();
      extract($vars);
      eval("?> $source <?php ");
   }

   public static function debug($view)
   {
      echo htmlentities(self::source($view));
   }

   public static function draw($view, $vars=array())
   {
      echo self::make($view, $vars);
   }

   public static function read($view, $vars=array())
   {
      return self::make($view, $vars);
   }
}