<?php
namespace Layout\Snake;

class Lexic
{
   public static function blockRules()
   {
      return array(
           'T_BLOCK_START' => ':block(\s+)'
         , 'T_BLOCK_END'   => ':endblock'
         , 'T_FULL_BLOCK'  => '/:block(\s+)(\S+)(\s+)(.*):endblock/Usx'
         , 'T_PARENT' => '/:parent/Usx'
      );
   }

   public static function inheritanceRules()
   {
      return array(
           'T_EXTENDS_FULL_PATH' => '/:extends(\s+)#(\S+)(\s+)/Usx'
         , 'T_EXTENDS' => '/:extends(\s+)(\S+)(\s+)/Usx'
      );
   }

   public static function parseRules()
   {
      $rules = [
           '/\{\{(.*)\}\}/U'      # short echo
         , '/:css(\s+)+(.+)/'     # short css include
         , '/:js(\s+)+(.+)/'      # short js include
         , '/:(if|elseif|for|foreach)(\s+)+(.*)/'  # short statements start
         , '/:(endif|endforeach|endfor)/' # short statements end
         , '/:else/'              # else
         , '/\$(\w+)\.(\w+)/'     # short array
         , '/@baseurl/'           # base url
         , '/:include(\s+)+(.*)/' # load
         , '/:year/' # year
      ];

      $replaces = [
           '<?php echo $1 ?>'
         , '<link rel="stylesheet" href="<?php echo \Demiphp\Url::base() ?>/public/css/$2.css">'
         , '<script src="<?php echo \Demiphp\Url::base() ?>/public/js/$2.js"></script>'
         , '<?php $1 ($3): ?>'
         , '<?php $1; ?>'
         , '<?php else: ?>'
         , '$$1["$2"]'
         , '\Demiphp\Url::base()'
         , '<?php self::template_include(\'$2\') ?>' // el eval esta dentro de la instancia de Snake
         , '<?php echo date(\'Y\'); ?>'
      ];

      return array($rules, $replaces);
   }
}